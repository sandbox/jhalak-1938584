<?php
/**
 * @file
 * Main include files for this module.
 */

/**
 * Insert log record into database.
 * 
 * @param Object $node 
 *   the node
 * @param String $type 
 *   type of the log (i.e. updated/inserted)
 */
function node_logger_log_record(&$node, $type = 'updated') {
  global $user;
  db_insert('node_log')
    ->fields(array(
      'nid'  => $node->nid,
      'vid'  => $node->vid,
      'uid'  => $user->uid,
      'type' => $type,
      'time' => REQUEST_TIME,
      )
    )
   ->execute();
}

/**
 * Delete log record from database.
 *
 * @param Object $node
 *   the node
 */
function node_logger_delete_log_record(&$node) {
  db_delete('node_log')
    ->condition('nid', $node->nid, '=')
    ->execute();
}

/**
 * Retrieve log record from database.
 *
 * @param Integer $nid
 *   the node id.
 *   
 * @return String[]
 *   Array of logs
 */
function node_logger_get_log_record($nid) {
  $result = db_select('node_log', 'nl')
  ->fields('nl', array('nid', 'vid', 'uid', 'type', 'time'))
  ->condition('nl.nid', $nid, '=')
  ->execute();
  return $result->fetchAll();
}

/**
 * Format timestamp to a date string. 
 * 
 * It implements hook_node_log_format_date()
 * and changes the value according to the implementation so that any external 
 * module can change the format from theme.
 *
 * @param Integer $timestamp
 *   the timestamp
 */
function node_logger_format_date(&$timestamp) {
  $date = $timestamp;
  drupal_alter('node_log_format_date', $timestamp);
  if ((int) $date === (int) $timestamp) {
    return format_date($date);
  }
  return $timestamp;
}
