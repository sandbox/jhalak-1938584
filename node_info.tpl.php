<?php 
/**
 * @file
 * node_info.tpl.php
 */
?>
<dl>
	<dt><?php print t('Created On') .       ': ' . node_logger_format_date($node->created); ?></dt>
	<dt><?php print t('Last modified On') . ': ' . node_logger_format_date($node->changed)?></dt>
</dl>
<br />
