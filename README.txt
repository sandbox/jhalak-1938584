Node Logger - Is a simple module that logs each update of the node 
made by any user.


Description
-----------------------------------------------
Its a very lightweight module that gives a simple overview of a node
containing information of user and update time made by user for a node
in a tabular format. This is very useful when the node revision feature
is not enabled but you still need to log just the updates of the nodes.


Installation
-----------------------------------------------
Download and install the module normally as you install
other contributed module.


Usage
-----------------------------------------------
After installation you have to give permission to "Access node log"
to your desired role. Now after log-in in user should see a "log" tab
when he visits in any node or additionally by browsing node/%nid/log.


Overriding
-----------------------------------------------
-- To override the date format you can do it by calling
   "hook_node_log_format_date_alter(&$timestamp)".
-- The themeable output is also can be override by redefining theme template.
