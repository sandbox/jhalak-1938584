<?php
/**
 * @file
 * Page callbacks for logging of contents.
 */

/**
 * Get the log of the users who edited the node.
 * 
 * @param Integer $nid
 *   The node id
 *   
 * @return String
 *   The HTML ouput of the log.
 */
function node_logger_node_log($nid) {
  module_load_include('inc', 'node_logger');

  $data = node_logger_get_log_record($nid);

  $output  = '';
  $output .= _node_logger_get_node_info($nid);
  $output .= _node_logger_get_node_logs($data);
  return $output;
}

/**
 * Get info of the node.
 * 
 * @param Integer $nid
 *   The node id
 *   
 * @return String
 *   The HTML ouput of the node info.
 */
function _node_logger_get_node_info($nid) {
  $node = node_load($nid);
  return theme('node_info', array('node' => $node));
}

/**
 * Get node log list.
 *
 * @param String[] $data
 *   The data array
 *
 * @return String
 *   The HTML tabular ouput of the log list.
 */
function _node_logger_get_node_logs($data) {
  $header = array(
    t('Updated By'),
    t('Updated On'),
  );
  $rows = array();
  foreach ($data as $log) {
    $rows[] = array(
      _node_logger_theme_username($log->uid),
      node_logger_format_date($log->time),
    );
  }

  $log_table = array(
    '#theme'      => 'table',
    '#header'     => $header,
    '#rows'       => $rows,
    '#empty'      => t('No update log exists'),
    '#prefix'     => '<div class="content-list-view">',
    '#suffix'     => '</div>',
    '#attributes' => array('class' => array('views-table')),
  );
  return drupal_render($log_table);
}

/**
 * Get themed output of username.
 *
 * @param Integer $uid
 *   The user id
 *
 * @return String
 *   The HTML ouput of the user name.
 */
function _node_logger_theme_username($uid) {
  $user = user_load($uid);
  $variables = array(
    'name'         => $user->name,
    'extra'        => '',
    'link_options' => array(),
    'link_path'    => 'user/' . $user->uid,
  );
  return theme_username($variables);
}
